# Empty paragraph killer

Empty paragraph killer is a filter module, helpful on sites which use
WYSIWYG editors.

People often hit the return key twice at the end of a paragraph. Most,
if not all site layouts manage the paragraph spacing, so the extra empty
paragraphs created can detract from the look and feel of a site. This
module filters out the empty paragraphs of all user-entered data on a
site. It does so by following the fundamental Drupal way -
non-destructively.

- For a full description of the module visit:
  [Project Page](https://www.drupal.org/project/emptyparagraphkiller).

- To submit bug reports and feature suggestions, or to track changes visit:
  [Issue Queue](https://www.drupal.org/project/issues/emptyparagraphkiller).


## Requirements

A WYSIWYG editor. If you are using Drupal without one, the "Line break
converter" in core is sufficient enough and you will achieve little or
no benefit in using this module.


## Installation

- Install as usual, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further
information.


## Configuration

- Go to your input formats `/admin/config/content/formats`
- Click the configure link next to the desired input format
- Click the checkbox next to 'Empty paragraph filter'.
- Under the "Filter processing order", ensure Empty paragraph killer is at the
  bottom of the list, unless you know there are other filters that need to be
  processed afterwards.

## Maintainers

- Richard Sheppard - [siliconmeadow](http://drupal.org/user/55284)
- Sang Lostrie - [baikho](https://www.drupal.org/u/baikho)
- skaught - [SKAUGHT](https://www.drupal.org/u/skaught)

**Sponsored by:**
- [Nomensa Ltd.](https://www.drupal.org/nomensa) - an internationally renowned
user experience (UX) agency with a reputation for exceptional thinking and
design delivery.
