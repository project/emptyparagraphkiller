<?php

namespace Drupal\Tests\emptyparagraphkiller\Kernel;

use Drupal\filter\FilterPluginCollection;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests Filter module filters individually.
 *
 * @group emptyparagraphkiller
 */
class EmptyParagraphKillerFilterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'filter', 'emptyparagraphkiller'];

  /**
   * The configured filter(s) set up for testing.
   *
   * @var \Drupal\filter\Plugin\FilterInterface[]
   */
  protected array $filters;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);

    $manager = $this->container->get('plugin.manager.filter');
    $bag = new FilterPluginCollection($manager, []);
    $this->filters = $bag->getAll();
  }

  /**
   * Tests the emptyparagraphkiller filter.
   *
   * @dataProvider emptyParagraphKillerFilterDataProvider
   */
  public function testEmptyParagraphKillerFilter($input, $expected): void {
    $filter = $this->filters['emptyparagraphkiller'];

    $test = function ($input) use ($filter) {
      $text = $filter->prepare($input, 'und');
      return $filter->process($text, 'und');
    };

    $this->assertSame($expected, $test($input)->getProcessedText());
  }

  /**
   * Data provider for testEmptyParagraphKillerFilter().
   *
   * @see testEmptyParagraphKillerFilter()
   */
  public function emptyParagraphKillerFilterDataProvider(): array {
    return [
      [
        '<p>Test content</p>',
        '<p>Test content</p>',
      ],
      [
        '<p>Test content</p><p></p>',
        '<p>Test content</p>',
      ],
      [
        '<p>Test content</p><p>&nbsp;</p>',
        '<p>Test content</p>',
      ],
      [
        '<p>Test content</p><p>&nbsp;</p><p></p>',
        '<p>Test content</p>',
      ],
      [
        '<p>Test content</p><p>&nbsp;</p><p>Test content 2</p>',
        '<p>Test content</p><p>Test content 2</p>',
      ],
      [
        '<p>Test content</p><p class="test">&nbsp;</p>',
        '<p>Test content</p>',
      ],
      [
        '<p>Test content</p><p class="test"></p>',
        '<p>Test content</p>',
      ],
    ];
  }

}
