<?php

namespace Drupal\Tests\emptyparagraphkiller\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the emptyparagraphkiller module in the UI.
 *
 * @group emptyparagraphkiller
 */
class EmptyParagraphKillerUiTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'emptyparagraphkiller_test',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->rootUser);
    $this->drupalCreateContentType(['type' => 'page']);
  }

  /**
   * Tests the emptyparagraphkiller module in the UI.
   */
  public function testEmptyParagraphKillerUi(): void {
    // Check that the plugin is available on a text format.
    $this->drupalGet('admin/config/content/formats/manage/emptyparagraphkiller_test');
    $this->assertSession()->pageTextContains('Empty Paragraph filter');

    $this->drupalGet('node/add/page');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
      'body[0][value]' => '<p>Test content</p><p>&nbsp;</p>',
    ], 'Save');

    $this->assertSession()->responseNotContains('<p>Test content</p><p>&nbsp;</p>');
  }

}
